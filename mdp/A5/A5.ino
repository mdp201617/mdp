#include <PinChangeInt.h>
#include <DualVNH5019MotorShield.h>
#include <SharpIR.h>

const int encoder0PinA = 3;
const int encoder0PinB = 5;

int encoderPos1 = 0;
int encoderPos2 = 0;

boolean moving = false;
boolean rotating = false;
boolean stopNormal = false;

char serialInput[10];

int speed = 360;

float threshold = 0.025;

SharpIR sharpLong1(A0, 20150);
SharpIR sharpShort1(A1, 1080);
SharpIR sharpShort2(A2, 1080);
SharpIR sharpShort3(A3, 1080);
SharpIR sharpShort4(A4, 1080);
SharpIR sharpShort5(A5, 1080);

DualVNH5019MotorShield md(2, 4, 6, 0, 7, 8, 12, 1);

void setup() {
  pinMode(encoder0PinA, INPUT); 
  digitalWrite(encoder0PinA, LOW);       // turn on pullup resistor
  pinMode(encoder0PinB, INPUT); 
  digitalWrite(encoder0PinB, LOW);       // turn on pullup resistor
  attachInterrupt(digitalPinToInterrupt(3), encoderIsp1, CHANGE); 
  PCintPort::attachInterrupt(5 ,&encoderIsp2, CHANGE);
  Serial.begin(9600);
  md.init();
//  callibrate();
//  delay(4000);
  moveForward(20);
//turn(16 * 1.027);
//turn(-12.0*1.04);
}

void loop() {
  int distance0 = sharpLong1.distance();
  int distance1 = sharpShort1.distance();
  int distance2 = sharpShort2.distance();
  int distance3 = sharpShort3.distance();
  int distance4 = sharpShort4.distance();
  int distance5 = sharpShort5.distance();
  int serialInputIndex = 0;
  char serialRawInput;
  char command;

  // Debug purpose
//  printSensorValues(distance0, distance1, distance2, distance3, distance4, distance5);

//  while (1){
//    if (Serial.available()){
//      serialRawInput = Serial.read();
//      serialInput[serialInputIndex] = serialRawInput;
//      serialInputIndex++;
//      if (serialRawInput == '|'){
//        serialInputIndex = 1;
//        break;
//      }
//    }
//  }
//
//  command = serialInput[0];
//
//  switch(command){
//    case 'F': 
//
//    break;
//    case 'B': 
//
//    break;
//    case 'L':
//
//    break;
//    case 'R':
//
//    break;
//    default:
//
//    break;
//  }
  int xxx = 0;
  if(!rotating && moving){
    if((distance1 <= 10 || distance2 <= 10 || distance3 <= 10) && !stopNormal){
      brake();
      moving = false;
      stopNormal = true;
      md.setBrakes(400, 400);
      delay(500);
      if(distance1 <= 15)
        xxx = 1;
      else if(distance2 <= 15)
        xxx = 0;
      else
        xxx =-1;
      turn(1.0 * 1.027);
      delay(1000);
      moveForward(2 + xxx);
      delay(1000);
      turn(-1.0 * 1.027);
      delay(1000);
      moveForward(4);
      delay(1500);
      turn(-1.0 * 1.027);
      delay(1000);
      moveForward(2 + xxx);
      delay(1000);
      turn(1.0 * 1.027);
      delay(1000);
      moveForward(1);
    }else{
      pid();
    }
  }
//  }else if(!rotating && !moving){
//    moveForward(1);
//  }
  
}

void pid(){
  if(!moving)
    return;
  double p; // = double(encoderPos2) / double(encoderPos1);
  int difference = encoderPos2 - encoderPos1;
  int newSpeed;
  p = 1.00 + difference / 150.00;
  newSpeed = speed * max(1.00 - threshold, min(1.00 + threshold, 1.00+p));
  Serial.println(encoderPos1);
  Serial.println(encoderPos2);
  Serial.println(p*1000.00);
//  int capSpeed = min(400, newSpeed);
  Serial.println(newSpeed);
  Serial.println("***********");
  md.setM2Speed(newSpeed * p);
}

void printSensorValues(int distance0, int distance1, int distance2, int distance3, int distance4, int distance5){
  Serial.print(distance0);
  Serial.print(" | ");
  Serial.print(distance1);
  Serial.print(" | ");
  Serial.print(distance2);
  Serial.print(" | ");
  Serial.print(distance3);
  Serial.print(" | ");
  Serial.print(distance4);
  Serial.print(" | ");
  Serial.print(distance5);
  Serial.print("\n");  
}

void turn(float direction){
  md.setBrakes(400, 400);
  moving = false;
  rotating = true;
  if(direction > 0){
    encoderPos1 = abs(direction) * 777;
    encoderPos2 = abs(direction) * 777;
  }else{
    encoderPos1 = abs(direction) * 785;
    encoderPos2 = abs(direction) * 785;
  }
  Serial.println(direction);
  Serial.println(encoderPos1);
  Serial.println(encoderPos2);
  Serial.println("*****");
  md.setSpeeds(direction * 200, direction * -200);
}

void turnCallibrate(int direction, float degree, float speed){
  int speed1;
  int speed2;
  if(direction > 0){
    encoderPos1 = degree * 777.0;
    encoderPos2 = degree * 777.0;
  }else{
    encoderPos1 = degree * 785.0;
    encoderPos2 = degree * 785.0;
  }
  speed1 = direction * (speed * 300.00);
  speed2 = direction * (speed * -300.00);
  md.setSpeeds(speed1, speed2); 
}

void callibrate(){
//  int distance0 = sharpLong1.distance();
  int distance1 = sharpShort1.distance();
  int distance2 = sharpShort2.distance();
  int distance3 = sharpShort3.distance();
//  int distance4 = sharpShort4.distance();
//  int distance5 = sharpShort5.distance();
  int success = 0;
  if((distance3 - distance1) > 1){
    Serial.println("Left greater");
    turnCallibrate(1, 0.25, 1);
    delay(40);
  }else if(distance1 - distance3 > 1){
    Serial.println("Right greater");
    turnCallibrate(-1, 0.25, 1);
    delay(40);
  }
  while(success < 4){
    distance1 = sharpShort1.distance();
    distance2 = sharpShort2.distance();
    distance3 = sharpShort3.distance();
    if(distance1 == distance2 && distance2 == distance3){
      md.setBrakes(400, 400);
      success = success + 1;
      delay(50);
      continue;
    }
    if(distance1 > distance2 ||  distance2 > distance3){
      turnCallibrate(-1, 0.1, 0.2);
    }else if(distance1 < distance2 ||  distance2 < distance3){
      turnCallibrate(1, 0.1, 0.2);
    }
    success = 0;
    delay(10);
  }
  Serial.println("DOne");
}

void brake(){
  md.setBrakes(400, 400);
}

void moveForward(int space){
  encoderPos1 = 588 * space;
  encoderPos2 = 588 * space;
  moving = true;
//  md.setSpeeds(speed, speed * 1.018);
  md.setM2Speed(speed); //  * 1.0055
  delay(8);
  md.setM1Speed(speed);
}

void encoderIsp1()
{
  encoderPos1--;
  
//  pid();
  
  if(rotating){
    if(encoderPos1 < 0){
      md.setM1Brake(400);
      rotating = false;
    }
  }else{
    if(encoderPos1 < 0){
      moving = false;
      md.setBrakes(400, 400);
    }
  }
}

void encoderIsp2()
{
  encoderPos2--;

  
//  pid();
  
  if(rotating){
    if(encoderPos2 < 0){
      md.setM2Brake(400);
      rotating = false;
    }
  }else{
    if(encoderPos2 < 0){
      moving = false;
      md.setBrakes(400, 400);
    }
  }
}
