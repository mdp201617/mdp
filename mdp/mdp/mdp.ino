#include <RunningMedian.h>
#include <PinChangeInt.h>
#include <DualVNH5019MotorShield.h>
#include <SharpIR.h>

// Encoder Pins Declaration
const int encoderPinLeft = 3;
const int encoderPinRight = 5;

// Encoder Position Count
volatile int encoderPosLeft = 0;
volatile int encoderPosRight = 0;

// General Speed
int fullSpeed = 200;
const int rotateSpeed = 200;

const int samplingSize = 9;

int sensorReadings[6] = {-1, -1, -1, -1, -1, -1};

int sensorFront[3] = {0,0,1};
int sensorLeft[3] = {1,0,0};
int sensorRight[3] = {0,0,0};

double newSpeed;
double encoderMod = 587.00;

double encoderDifference = 0;
double integral = 0;
double pidd = 0;

String rawInput;
String fastestPath;

int lastLeftReading = 0;

const int movementDelay = 40;

int x = 1;
int y = 1;
char bearing = 'N';
bool started = false;

int northHeading = 0;

const int headingThreshold = 1;

RunningMedian sample0 = RunningMedian(samplingSize);
RunningMedian sample1 = RunningMedian(samplingSize);
RunningMedian sample2 = RunningMedian(samplingSize);
RunningMedian sample3 = RunningMedian(samplingSize);
RunningMedian sample4 = RunningMedian(samplingSize);
RunningMedian sample5 = RunningMedian(samplingSize);
RunningMedian sample6 = RunningMedian(samplingSize);
RunningMedian sampleRPML = RunningMedian(9);
RunningMedian sampleRPMR = RunningMedian(9);

RunningMedian sampleCompass = RunningMedian(9);

// Sensor Declaration
SharpIR sharpLong1(A0, 20150); // Right side front sensor
SharpIR sharpShort1(A1, 1080); // Front right side sensor
SharpIR sharpShort2(A2, 1080); // Front middle sensor
SharpIR sharpShort3(A3, 1080); // Front left side sensor
SharpIR sharpShort4(A4, 1080); // Left side front sensor
SharpIR sharpShort5(A5, 1080); // Left side rear sensor

// Motor Shield Declaration
DualVNH5019MotorShield md(2, 4, 6, 0, 7, 8, 12, 1);

//JH stuff
float magicValue = 1.060; //long 1.060
float magicValue2 = 0.9545;  //long 0.9545
float targetRPM;
float timePWML;
float timePWMR;
float widthL;
float widthR;
float rpmL;
float rpmR;
float currentSpeedL;
float currentSpeedR;
float diffRPML;
float diffRPMR;

int moveCount = 0;

void setup() {
  // Encoder Pin Initialization
  pinMode(encoderPinLeft, INPUT);
  pinMode(encoderPinRight, INPUT); 
  // turn on pullup resistor
  digitalWrite(encoderPinLeft, LOW);
  digitalWrite(encoderPinRight, LOW); 
  // ISP for encoder pins
  // Using PCIntPort to convert pin to interrupt sensitive
  PCintPort::attachInterrupt(encoderPinLeft ,&encoderIspLeft, CHANGE);
  PCintPort::attachInterrupt(encoderPinRight ,&encoderIspRight, CHANGE);
//  PWM_Mode_Setup();
  // Initialize serial at baud rate 9600
  Serial.begin(230400);
  // Init motor shield module
  md.init();

  // Run custom code
  initialize();
}

void initialize(){
//  rawInput = "E,^1,";
//  rawInput = "E,<,B,";
//  rawInput = "S,L,B,L,B,L,B,L,B,";
  rawInput = "S,R,B,"; //R,B,R,B,R,B,";
  rawInput = "S,L,B,";
//  rawInput = "S,F1,b,l,b,F1,b,";
  rawInput = "S,F1,b,>,b,";
  rawInput = "S,F1,b,<,b,";
  rawInput = "S,F11,B,";
  rawInput = "E,F1,B,F1,B,F1,B,F1,B,F1,B,F1,B,F1,B,"; //F1,B,F1,B,";//F1,F1,B,";
//  rawInput = "S,F100,b,F100,b,"; //r,b,F1,b,l,b,F1,b,r,b,F5,b,";
//  rawInput = "S,F5,b,>,b,F6,b,<,b,F9,b,>,b,F3,b,"; //r,b,F1,b,l,b,F1,b,r,b,F5,b,";
//  md.setSpeeds(500,500);
//  delay(10000);
}

void loop() {
//  printSensorValues();
  listenToSerial();
}

void listenToSerial(){
  while(Serial.available()){
    char character = Serial.read(); // Receive a single character from the software serial port
    rawInput.concat(character); // Add the received character to the receive buffer
    if (character == '\n'){
      Serial.println(rawInput);
      break;
    }
  }
  if(rawInput.length() <= 0)
    return;
  String currentInstruction = "";
  for (int i = 0; i < rawInput.length(); i++) {
    if (rawInput.substring(i, i+1) == ",") {
      currentInstruction = rawInput.substring(0, i);
      rawInput = rawInput.substring(i+1);
      break;
    }
  }
  char command = currentInstruction[0];
  int steps = 1;
  currentInstruction.remove(0,1);
  switch(command){
    case 'H':
      fastestPath = rawInput;
      rawInput = "";
      fullSpeed = 360;
      encoderMod = 550;
      break;
    case 'o':
      rawInput = fastestPath;
      fastestPath = "";
      break;
    case 'E':
      fullSpeed = 360;
      encoderMod = 520;
      break;
    case 'S':
      fullSpeed = 360;
      encoderMod = 550;
      break;
    case 'F': 
      steps = currentInstruction.toInt();
      steps = (steps > 0) ? steps : 1;
      moveForward(steps, true);
      break;
    case 'f': 
      moveForward(currentInstruction.toInt(), false);
      break;
    case 'B': 
      md.setBrakes(400, 400);
      printSensorValues();
      delay(movementDelay);
      break;
    case 'b': 
      md.setBrakes(400, 400);
      delay(movementDelay+30);
      break;
    case 'L':
      rotateLeft(90);
      break;
    case 'R':
      rotateRight(90);
      break;
    case 'Z':
      moveForward(-1*currentInstruction.toInt(), true);
      break;
    case 'C':
      calibrate();
      break;
    case 'r':
      smoothTurn(1);
      break;
    case 'l':
      smoothTurn(-1);
      break;
    case '>':
      smallSmoothTurn(1);
      break;
    case '<':
      smallSmoothTurn(-1);
      break;
//    case '>':
//      smoothTurnWithoutBreak(1);
//      break;
//    case '<':
//      smoothTurnWithoutBreak(-1);
//      break;
    case '}':
      switchLane(1);
      break;
    case '{':
      switchLane(-1);
      break;
    case '^':
      moveForwardUntilObstacles(currentInstruction.toInt());
      break;
    default:
      md.setBrakes(400, 400);
      delay(movementDelay);
      break;
  }
  pidd = 0;
  integral = 0;
  encoderDifference = 0;
}

void printSensorValues(){
  collectSensorData();
  Serial.print("pr");
  Serial.print(sensorReadings[0]);
  Serial.print(',');
  Serial.print(sensorReadings[1]);
  Serial.print(',');
  Serial.print(sensorReadings[2]);
  Serial.print(',');
  Serial.print(sensorReadings[3]);
  Serial.print(',');
  Serial.print(sensorReadings[4]);
  Serial.print(',');
  Serial.println(sensorReadings[5]); 
}

void printSensorValuesWithoutCollection(String actionCommand){
  Serial.print('p');
  Serial.print(actionCommand);
  Serial.print(sensorReadings[0]);
  Serial.print(',');
  Serial.print(sensorReadings[1]);
  Serial.print(',');
  Serial.print(sensorReadings[2]);
  Serial.print(',');
  Serial.print(sensorReadings[3]);
  Serial.print(',');
  Serial.print(sensorReadings[4]);
  Serial.print(',');
//  Serial.println(sensorReadings[5]);
  Serial.print(sensorReadings[5]);
  Serial.print(',');
  Serial.print('[');
  Serial.print(x);
  Serial.print(',');
  Serial.print(y);
  Serial.println(']');
}

void collectSensorData(){
  int distance0;
  int distance1;
  int distance2;
  int distance3;
  int distance4;
  int distance5;
  int i=0;

  lastLeftReading = sample4.getMedian();
  
  sample0.clear();
  sample1.clear();
  sample2.clear();
  sample3.clear();
  sample4.clear();
  sample5.clear();
  for(i=0; i<samplingSize; i++){
    distance0 = sharpLong1.distance();
    distance1 = sharpShort1.distance();
    distance2 = sharpShort2.distance();
    distance3 = sharpShort3.distance();
    distance4 = sharpShort4.distance();
    distance5 = sharpShort5.distance();
    sample0.add(distance0);
    sample1.add(distance1);
    sample2.add(distance2);
    sample3.add(distance3);
    sample4.add(distance4);
    sample5.add(distance5);
  }
  
  sensorReadings[0] = sample0.getMedian();
  sensorReadings[1] = sample1.getMedian();
  sensorReadings[2] = sample2.getMedian();
  sensorReadings[3] = sample3.getMedian();
  sensorReadings[4] = sample4.getMedian();
  sensorReadings[5] = sample5.getMedian();
}

void encoderIspLeft(){
  encoderPosLeft--;
}

void encoderIspRight(){
  encoderPosRight--;
}

void rotateLeft(int angle){
  double pidValue = 1.00;
  encoderPosLeft = 753 * (angle / 90.0);
  encoderPosRight = 752 * (angle / 90.0);
  magicValue = 1.15; //1.120;//
  magicValue2 = 0.95;
  currentSpeedL = 75;
  currentSpeedR = 75*magicValue;
  md.setSpeeds(-1 * currentSpeedL, currentSpeedR);
  
  targetRPM = rotateSpeed / 3.00;
  while(encoderPosLeft > 0 && encoderPosRight > 0){
    rpmPidRotate(-1);
  }
  md.setBrakes(400, 400);
}

void rotateRight(int angle){
  double pidValue = 1.00;
  encoderPosLeft = 754 * (angle / 90.0);
  encoderPosRight = 755 * (angle / 90.0);
  magicValue = 1.15; //1.120;//
  magicValue2 = 0.95;
  currentSpeedL = 75;
  currentSpeedR = 75*magicValue;
  md.setSpeeds(currentSpeedL, -1 * currentSpeedR);
  
  targetRPM = rotateSpeed / 3.00;
  while(encoderPosLeft > 0 && encoderPosRight > 0){
    rpmPidRotate(1);
  }
  md.setBrakes(400, 400);
}

double pid(){
  pidd = integral;
  integral = encoderDifference;
  encoderDifference =  encoderPosLeft-encoderPosRight;
  double result = 1.0 * encoderDifference + 2.0 * integral + 1.1 * pidd;
  return max(0.0000001, 1.00 + result / 200.00);
}

double pidLeft(){
  pidd = integral;
  integral = encoderDifference;
  encoderDifference =  encoderPosRight-encoderPosLeft;
  double result = 1.0 * encoderDifference + 2.0 * integral + 1.1 * pidd;
  return max(0.0000001, 1.00 + result / 200.00);
}

void rpmPid(){
  rpmL = 0;
  rpmR = 0;
  calculateRpm();
  diffRPML = rpmL/targetRPM;
  diffRPMR = rpmR/targetRPM;
  if(diffRPML < 0.99){
    currentSpeedL = (currentSpeedL + max(3, 25*(1-diffRPML)));
  }else if(diffRPML > 1.01){
    currentSpeedL = (currentSpeedL + min(-3, 25*(1-diffRPML)));
  }
  if(diffRPMR < 0.99){
    currentSpeedR = (currentSpeedR + max(3, 25*(1-diffRPMR)));
  }else if(diffRPMR > 1.01){
    currentSpeedR = (currentSpeedR + min(-3, 25*(1-diffRPMR))); 
  }
  md.setSpeeds(currentSpeedL,currentSpeedR);
}

void rpmPidRotate(int direction){
  rpmL = 0;
  rpmR = 0;
  calculateRpm();
  diffRPML = rpmL/targetRPM;
  diffRPMR = rpmR/targetRPM;
  if(diffRPML < 0.99){
    currentSpeedL = (currentSpeedL + max(3, 25*(1-diffRPML)));
  }else if(diffRPML > 1.01){
    currentSpeedL = (currentSpeedL + min(-3, 25*(1-diffRPML)));
  }
  if(diffRPMR < 0.99){
    currentSpeedR = (currentSpeedR + max(3, 25*(1-diffRPMR)));
  }else if(diffRPMR > 1.01){
    currentSpeedR = (currentSpeedR + min(-3, 25*(1-diffRPMR))); 
  }
  md.setSpeeds(direction * currentSpeedL, -1 * direction * currentSpeedR);
}

void calculateRpm(){
  timePWML = 0;
  timePWMR = 0;

  for(int i=0; i<7; i++){
    timePWML = pulseIn(encoderPinLeft,HIGH);
    timePWMR = pulseIn(encoderPinRight,HIGH);
    sampleRPML.add(timePWML);
    sampleRPMR.add(timePWMR);
    }
    
  widthL = sampleRPML.getMedian() * 2;
  widthR = sampleRPMR.getMedian() * 2;
  rpmL = 60000000/(widthL * 562.25) * magicValue2;
  rpmR = 60000000/(widthR * 562.25);
}

void moveForward(double square, bool slowStart){
  encoderPosLeft = abs(square) * (encoderMod + (abs(square) - 1.00) * 3.11111);
  encoderPosRight = abs(square) * (encoderMod + (abs(square) - 1.00) * 3.11111);
//  if(fullSpeed >= 350){
    if(square >= 3.00){
      magicValue = 1.052;//1.060;
      magicValue2 = 0.960; //0.9545;
//    }else if(square >= 2.00){
//      magicValue = 1.0505;
//      magicValue2 = 0.9545;
    }else{
      magicValue = 1.125; //1.120;//
      magicValue2 = 0.92;
    }
//  }
//  else{
//    if(square >= 3.00){
//      magicValue = 1.085;//1.060;
//      magicValue2 = 0.9545;
//    }else if(square >= 2.00){
//      magicValue = 1.0505;
//      magicValue2 = 0.9545;
//    }else{
//      magicValue = 1.14; //1.120;//
//      magicValue2 = 0.92;
//    }
//  }
  if(slowStart){
    currentSpeedL = 100;
    currentSpeedR = 100*magicValue;
    md.setSpeeds(currentSpeedL,currentSpeedR);
  }else{
    encoderPosLeft += square * 2;
    encoderPosRight += square * 2;
  }
  
  targetRPM = (square > 0 ? 1 : -1 ) * fullSpeed / 3.00;
  while(encoderPosLeft > 0 && encoderPosRight > 0){
    rpmPid();
  }
  md.setBrakes(400, 400);
}

int isBlock(int sensor){
  int reading = sensorReadings[sensor];
  switch(sensor){
    case 1:
      if(reading < 11)
        return 1;
      return 0;
      break;
    case 2:
      if(reading <= 10)
        return 1;
      return 0;
      break;
    case 3:
      if(reading < 11)
        return 1;
      return 0;
      break;
    case 4:
      if(reading < 15)
        return 1;
      return 0;
      break;
    case 5:
      if(reading < 11)
        return 1;
      return 0;
      break;
    default:
      return 0;
      break;
  }
  return 0;
}

void updateSensorDataAfterMovement(int direction){
  int temporaryInt;
  switch(direction){
    case 1: // right
      sensorLeft[0] = sensorFront[0];
      sensorLeft[1] = sensorFront[1];
      sensorLeft[2] = sensorFront[2];
      sensorFront[0] = 0;
      sensorFront[1] = 0;
      sensorFront[2] = 0;
      if(bearing == 'N'){
        bearing = 'E';
      }else if(bearing == 'E'){
        bearing = 'S';
      }else if(bearing == 'S'){
        bearing = 'W';
      }else{
        bearing = 'N';
      }
    break;
    case 0: // forward
      started = true;
      moveCount++;
      sensorLeft[2] = sensorLeft[1];
      sensorLeft[1] = sensorLeft[0];
      sensorLeft[0] = 0;
      sensorRight[2] = sensorRight[1];
      sensorRight[1] = sensorRight[0];
      sensorRight[0] = 0;
      if(bearing == 'N'){
        if(y <= 17)
          y++;
      }else if(bearing == 'E'){
        if(x <= 12)
          x++;
      }else if(bearing == 'S'){
        if(y >= 2)
          y--;
      }else{
        if(x >= 2)
          x--;
      }
    break;
    case 2:
      if(bearing == 'N'){
        bearing = 'W';
      }else if(bearing == 'W'){
        bearing = 'S';
      }else if(bearing == 'S'){
        bearing = 'E';
      }else{
        bearing = 'N';
      }
    break;
    default:
    break;
  }
}

void updateSensorDataAfterCollection(){
  sensorFront[0] = isBlock(1);
  sensorFront[1] = isBlock(2);
  sensorFront[2] = isBlock(3);
  sensorLeft[0] = isBlock(4);
  sensorRight[1] = isBlock(5);
}

void moveForwardUntilObstacles(int wall){
//   
//  int countcount = 0;
//  for(countcount=0; countcount < 17; countcount++){
//    Serial.println("pw1025,147,141,1972,7,1074");
//    delay(500);
//  }
//  Serial.println("pd1025,7,7,7,7,1074");
//    delay(500);
//  for(countcount=0; countcount < 12; countcount++){
//    Serial.println("pw1025,147,141,1972,7,1074");
//    delay(500);
//  }
//  Serial.println("pd1025,7,7,7,7,1074");
//    delay(500);
//  for(countcount=0; countcount < 17; countcount++){
//    Serial.println("pw1025,147,141,1972,7,1074");
//    delay(500);
//  }
//  Serial.println("pd1025,7,7,7,7,1074");
//    delay(500);
//  for(countcount=0; countcount < 12; countcount++){
//    Serial.println("pw1025,147,141,1972,7,1074");
//    delay(500);
//  }
//  Serial.println("pt1025,7,7,7,7,1074");
//    delay(500);
//  return;
  
  started = false;
  String nextAction = "i";
  int count=0;
  int leftCount = 0;

  bool justLeave = false;
  bool forceForward = false;
  bool mustHaveLeftWall = false;
  bool findMeAWallInfront = false;
  bool laterCheckLeftWall = false;

  bool alwaysForwardThenRightWhenObstacleInFront = false;

  bool completeCalibration = false;

  // 0 - empty
  // 1 - blocked
  sensorLeft[0] = 0;
  sensorLeft[1] = 0;
  sensorLeft[2] = 0;
  sensorFront[0] = 0;
  sensorFront[1] = 0;
  sensorFront[2] = 0;
  sensorRight[0] = 0;
  sensorRight[1] = 0;
  sensorRight[2] = 0;
  
  while(true){
    if(moveCount >= 6 && ((x<=2 && y==1) || (x==1 && y<=2))){
      collectSensorData();
      updateSensorDataAfterCollection();
      if(sensorFront[0] == 1 || sensorFront[1] == 1 || sensorFront[2] == 1){
        switch(bearing){
          case 'N':
            rotateRight(90);
            delay(movementDelay);
          case 'W':
            rotateLeft(90);
            delay(movementDelay);
          case 'S':
            calibrate();
            rotateRight(90);
            delay(movementDelay);
            calibrate();
            rotateRight(90);
            delay(movementDelay);
            break;
          case 'E':
            rotateRight(90);
            delay(movementDelay);
            calibrate();
            rotateRight(90);
            delay(movementDelay);
            calibrate();
            rotateRight(90);
            delay(movementDelay);
            break;
          default:break;
        }
        collectSensorData();
        printSensorValuesWithoutCollection("t");
//        Serial.println(String('p') + "t" + String(sensorReadings[0]) + String(',') + String(sensorReadings[1]) + String(',') + String(sensorReadings[2]) + String(',') + String(sensorReadings[3]) + String(',') + String(sensorReadings[4]) + String(',') + String(sensorReadings[5])); 
        return;
      }
    }
    nextAction = "i";
    if(findMeAWallInfront){
      findMeAWallInfront = false;
      updateSensorDataAfterMovement(2);
      nextAction = "a";
      rotateLeft(90);
      md.setBrakes(400, 400);
      printSensorValuesWithoutCollection(nextAction);
//      Serial.println(String('p') + nextAction + String(sensorReadings[0]) + String(',') + String(sensorReadings[1]) + String(',') + String(sensorReadings[2]) + String(',') + String(sensorReadings[3]) + String(',') + String(sensorReadings[4]) + String(',') + String(sensorReadings[5])); 
      delay(movementDelay);
      collectSensorData();
      updateSensorDataAfterCollection();
      while(!(sensorFront[0] == 1 || sensorFront[1] == 1 || sensorFront[2] == 1)){ // if all empty
        if((x<=2 && y==1) && (x==1 && y<=2))
          break;
        updateSensorDataAfterMovement(0);
        nextAction = "w";
        moveForward(1, true);
        md.setBrakes(400, 400);
        printSensorValuesWithoutCollection(nextAction);
//        Serial.println(String('p') + nextAction + String(sensorReadings[0]) + String(',') + String(sensorReadings[1]) + String(',') + String(sensorReadings[2]) + String(',') + String(sensorReadings[3]) + String(',') + String(sensorReadings[4]) + String(',') + String(sensorReadings[5])); 
        delay(movementDelay);
        collectSensorData();
        updateSensorDataAfterCollection();
        nextAction = "i";
      }
      updateSensorDataAfterMovement(1);
      nextAction = "d";
      rotateRight(90);
      md.setBrakes(400, 400);
      printSensorValuesWithoutCollection(nextAction);
//      Serial.println(String('p') + nextAction + String(sensorReadings[0]) + String(',') + String(sensorReadings[1]) + String(',') + String(sensorReadings[2]) + String(',') + String(sensorReadings[3]) + String(',') + String(sensorReadings[4]) + String(',') + String(sensorReadings[5])); 
      delay(movementDelay);
      nextAction = "i";
    }
    if(alwaysForwardThenRightWhenObstacleInFront){
      collectSensorData();
      updateSensorDataAfterCollection();
      updateSensorDataAfterMovement(1);
      nextAction = "d";
      rotateRight(90);
      md.setBrakes(400, 400);
      printSensorValuesWithoutCollection(nextAction);
//      Serial.println(String('p') + nextAction + String(sensorReadings[0]) + String(',') + String(sensorReadings[1]) + String(',') + String(sensorReadings[2]) + String(',') + String(sensorReadings[3]) + String(',') + String(sensorReadings[4]) + String(',') + String(sensorReadings[5])); 
      delay(movementDelay);
      updateSensorDataAfterMovement(1);
      nextAction = "d";
      rotateRight(90);
      md.setBrakes(400, 400);
      printSensorValuesWithoutCollection(nextAction);
//      Serial.println(String('p') + nextAction + String(sensorReadings[0]) + String(',') + String(sensorReadings[1]) + String(',') + String(sensorReadings[2]) + String(',') + String(sensorReadings[3]) + String(',') + String(sensorReadings[4]) + String(',') + String(sensorReadings[5])); 
      delay(movementDelay);
      collectSensorData();
      updateSensorDataAfterCollection();
      while(!(sensorFront[0] == 1 || sensorFront[1] == 1 || sensorFront[2] == 1)){ // if all empty
        updateSensorDataAfterMovement(0);
        nextAction = "w";
        moveForward(1, true);
        md.setBrakes(400, 400);
        printSensorValuesWithoutCollection(nextAction);
//        Serial.println(String('p') + nextAction + String(sensorReadings[0]) + String(',') + String(sensorReadings[1]) + String(',') + String(sensorReadings[2]) + String(',') + String(sensorReadings[3]) + String(',') + String(sensorReadings[4]) + String(',') + String(sensorReadings[5])); 
        delay(movementDelay);
        collectSensorData();
        updateSensorDataAfterCollection();
        nextAction = "i";
      }
      updateSensorDataAfterMovement(1);
      nextAction = "d";
      rotateRight(90);
      md.setBrakes(400, 400);
      printSensorValuesWithoutCollection(nextAction);
//      Serial.println(String('p') + nextAction + String(sensorReadings[0]) + String(',') + String(sensorReadings[1]) + String(',') + String(sensorReadings[2]) + String(',') + String(sensorReadings[3]) + String(',') + String(sensorReadings[4]) + String(',') + String(sensorReadings[5])); 
      delay(movementDelay);
      alwaysForwardThenRightWhenObstacleInFront = false;
      nextAction = "i";
    }
    collectSensorData();
    updateSensorDataAfterCollection();
    if(!completeCalibration && sensorFront[0] == 1 && sensorFront[1] == 1 && sensorFront[2] == 1 && sensorLeft[0] == 1 && sensorLeft[1] == 1 && sensorLeft[2] == 1){ // front & left complete wall
      calibrateDistance();
      rotateLeft(90);
      md.setBrakes(400, 400);
      delay(movementDelay);
      calibrateDistance();
      rotateRight(90);
      md.setBrakes(400, 400);
      delay(movementDelay);
      calibrate();
      collectSensorData();
      updateSensorDataAfterCollection();
      completeCalibration = true;
    }else if(sensorFront[0] == 1 && sensorFront[1] == 1 && sensorFront[2] == 1){ // front
      calibrate();
      collectSensorData();
      updateSensorDataAfterCollection();
    }else if(sensorLeft[0] == 1 && sensorLeft[1] == 1 && sensorLeft[2] == 1  && (sensorReadings[4] <= 8 || sensorReadings[4] >= 12)){
      rotateLeft(90);
      md.setBrakes(400, 400);
      delay(movementDelay);
      calibrate();
      rotateRight(90);
      md.setBrakes(400, 400);
      delay(movementDelay);
      collectSensorData();
      updateSensorDataAfterCollection();
    }else if(sensorRight[1] == 1 && sensorRight[2] == 1 && sensorReadings[5] > 8 && sensorReadings[5] < 11){
//      advancedCalibration(false, true, true);
    }
    if(laterCheckLeftWall){
      if(sensorLeft[0] == 0){
        findMeAWallInfront = true;
        continue;
      }
      laterCheckLeftWall = false;
    }
    if(forceForward || sensorLeft[0] == 1 || sensorLeft[1] == 1 || sensorLeft[2] == 1){//gt obstacles on left
      if(!forceForward && (sensorFront[0] == 1 || sensorFront[1] == 1 || sensorFront[2] == 1)){//gt obstacles in front
        if(!(sensorFront[0] == 1 && sensorFront[1] == 1 && sensorFront[2] == 1) && sensorLeft[0] == 1 && sensorLeft[1] == 1 && sensorLeft[2] == 1){
          rotateLeft(90);
          md.setBrakes(400, 400);
          delay(movementDelay);
          calibrate();
          rotateRight(90);
          md.setBrakes(400, 400);
          delay(movementDelay);
          collectSensorData();
          updateSensorDataAfterCollection();
        }
        updateSensorDataAfterMovement(1);
        rotateRight(90);
        nextAction = "d";
      }else{ // nth in front
        if(forceForward){
          forceForward = false;
          laterCheckLeftWall = true;
        }
        if(sensorLeft[0] == 0 && sensorLeft[1] == 0){
          justLeave = true;
        }else{
          justLeave = false;
        }
        completeCalibration = false;
        updateSensorDataAfterMovement(0);
        moveForward(1, true);
        nextAction = "w";
      }
      leftCount = 0;
    }else{
      if(justLeave){
        justLeave = false;
        forceForward = true;
      }
      if(mustHaveLeftWall){
        findMeAWallInfront = true;
        mustHaveLeftWall = false;
      }
      if(leftCount >= 3){
        alwaysForwardThenRightWhenObstacleInFront = true;
        leftCount = 0;
        continue;
      }
      updateSensorDataAfterMovement(2);
      if(x > 10)
        rotateLeft(89);
      else
      rotateLeft(90);
      nextAction = "a";
      leftCount++;
    }
    md.setBrakes(400, 400);
    printSensorValuesWithoutCollection(nextAction);
    delay(movementDelay);
    nextAction = "i";
  }
}

void smoothTurn(int direction){
  double pidValue = 1.00;
  double sSpeed = 182.00;
  if(direction > 0){
    encoderPosLeft = 2585;
    encoderPosRight = 912;
  }else{
    encoderPosRight = 2600; //2595; //2585;
    encoderPosLeft = 905; //912;
    sSpeed = 180; //170
  }
  while(encoderPosLeft > 0 || encoderPosRight > 0){
      pidValue = direction > 0 ? pid() : pidLeft();
      newSpeed = min(400, max(sSpeed, sSpeed * pidValue));
      if(direction > 0)
        md.setSpeeds(round(newSpeed), sSpeed);
      else
        md.setSpeeds(sSpeed, round(newSpeed));
  }
}

void smallSmoothTurn(int direction){
  double pidValue = 1.00;
  double sSpeed = 60.00;
  if(direction > 0){
    encoderPosLeft = 1700; //1680;
    encoderPosRight = 80; //70;
//    md.setM2Brake(400);
  }else{
    encoderPosRight = 1685; //1680;
    encoderPosLeft = 80; //70;
//    md.setM1Brake(400);
  }
  while(encoderPosLeft > 0 || encoderPosRight > 0){
    pidValue = direction > 0 ? pid() : pidLeft();
    newSpeed = min(430, max(sSpeed, sSpeed * pidValue));
    if(direction > 0){
      md.setSpeeds(round(newSpeed), sSpeed);
      if(encoderPosLeft <= 7)
        break;
    }else{
      md.setSpeeds(sSpeed, round(newSpeed));
      if(encoderPosRight <= 7)
        break;
    }
  }
}

void smoothTurnWithoutBreak (int direction){
  double pidValue = 1.00;
  double sSpeed = 176.00;

  moveForward(1.1, false);
  
  if(direction > 0){
    encoderPosLeft = 2620;
    encoderPosRight = 812;
  }else{
    encoderPosRight = 2620;
    encoderPosLeft = 812;
    sSpeed = 146;
  }
  while(encoderPosLeft > 0 || encoderPosRight > 0){
      pidValue = direction > 0 ? pid() : pidLeft();
      newSpeed = min(400, max(sSpeed, sSpeed * pidValue));
      if(direction > 0)
        md.setSpeeds(round(newSpeed), sSpeed);
      else
        md.setSpeeds(sSpeed, round(newSpeed));
  }

  moveForward(1.2, false);
}

void switchLane(int direction){
  double pidValue = 1.00;
  double sSpeed = 60.00;

  moveForward(0.4, false);
  
  if(direction > 0){
    encoderPosLeft = 1500;
    encoderPosRight = 300;
  }else{
    encoderPosLeft = 280;
    encoderPosRight = 1530;
    sSpeed = 35;
  }
  while(encoderPosLeft > 0 || encoderPosRight > 0){
      pidValue = direction > 0 ? pid() : pidLeft();
      newSpeed = min(400, max(sSpeed, sSpeed * pidValue));
      if(direction > 0)
        md.setSpeeds(round(newSpeed), sSpeed);
      else
        md.setSpeeds(sSpeed, round(newSpeed));
//      Serial.println(String(pidValue) + " - " + String(round(newSpeed)));
  }

  moveForward(0.5, false);

  if(direction > 0){
    encoderPosLeft = 300;
    encoderPosRight = 1515;
    sSpeed = 35;
  }else{
    encoderPosLeft = 1370;
    encoderPosRight = 280;
    sSpeed = 70;
  }
  while(encoderPosLeft > 0 || encoderPosRight > 0){
      pidValue = direction < 0 ? pid() : pidLeft();
      newSpeed = min(400, max(sSpeed, sSpeed * pidValue));
      if(direction < 0)
        md.setSpeeds(round(newSpeed), sSpeed);
      else
        md.setSpeeds(sSpeed, round(newSpeed));
  }

  moveForward(1.3, false);
  
}

void calibrate(){
  int distance1 = sharpShort1.distance();
  int distance2 = sharpShort2.distance();
  int distance3 = sharpShort3.distance();
  int difference = distance1-distance3;
  int lastDifference = difference;
  int i=0;
  int originalSpeed = fullSpeed;

  sample1.clear();
  sample2.clear();
  sample3.clear();
  for(i=0; i<samplingSize; i++){
    distance1 = sharpShort1.distance();
    distance2 = sharpShort2.distance();
    distance3 = sharpShort3.distance();
    sample1.add(distance1);
    sample2.add(distance2);
    sample3.add(distance3);
  }
  distance1 = sample1.getMedian();
  distance2 = sample2.getMedian();
  distance3 = sample3.getMedian();
  difference = distance1-distance3;

  if(distance1 > 15 || distance2 > 15 || distance3 > 15){
    md.setSpeeds(-100,-100);
    delay(300);
    md.setBrakes(400, 400);
    delay(50);  
  }
    
  while (lastDifference != 0 || sample2.getMedian() != 8){
    sample1.clear();
    sample2.clear();
    sample3.clear();
    for(i=0; i<samplingSize; i++){
      distance1 = sharpShort1.distance();
      distance2 = sharpShort2.distance();
      distance3 = sharpShort3.distance();
      sample1.add(distance1);
      sample2.add(distance2);
      sample3.add(distance3);
    }
    distance1 = sample1.getMedian();
    distance2 = sample2.getMedian();
    distance3 = sample3.getMedian();
    lastDifference = difference;
    difference = distance1-distance3;
    if(difference > 0){
      md.setSpeeds(-75, 74);
    }else if(difference < 0){
      md.setSpeeds(75, -74);
    }
    delay(40);
    md.setBrakes(400, 400);
    
    if(distance2 > 8){
      md.setSpeeds(100,100);
    }else{
      md.setSpeeds(-100,-100);
    }
    delay(50);
    md.setBrakes(400, 400);   
  } 
  md.setBrakes(400, 400);
  delay(movementDelay);
  fullSpeed = 100;
  moveForward(0.16, true);
  fullSpeed = originalSpeed;
  md.setBrakes(400, 400);
  delay(movementDelay);
}

void calibrateDistance(){
  int distance2 = sharpShort2.distance();
  int i=0;
  int originalSpeed = fullSpeed;

  sample2.clear();
  for(i=0; i<samplingSize; i++){
    distance2 = sharpShort2.distance();
    sample2.add(distance2);
  }

  if(distance2 > 15){
    md.setSpeeds(-100,-100);
    delay(300);
    md.setBrakes(400, 400);
    delay(50);  
  }
    
  while (sample2.getMedian() != 8){
    sample2.clear();
    for(i=0; i<samplingSize; i++){
      distance2 = sharpShort2.distance();
      sample2.add(distance2);
    }
    distance2 = sample2.getMedian();
    
    if(distance2 > 15){
      md.setSpeeds(-100,-100);
      delay(300);
      md.setBrakes(400, 400);
      delay(50);  
    }
    if(distance2 > 8){
      md.setM1Speed(120);
      md.setM2Speed(120);
    }else{
      md.setM1Speed(-120);
      md.setM2Speed(-120);
    }
    delay(movementDelay);
    md.setBrakes(400, 400);   
  } 
  md.setBrakes(400, 400);
  delay(movementDelay);
  fullSpeed = 100;
  moveForward(0.15, true);
  fullSpeed = originalSpeed;
  md.setBrakes(400, 400);
  delay(movementDelay);
}

void advancedCalibration(bool left, bool middle, bool right){
  if((left && !middle && !right) || (!left && middle && !right) || (!left && !middle && right) || (!left && !middle && !right))
    return;
  if(left && middle && right)
    calibrate();
  int distance1 = sharpShort1.distance();
  int distance2 = sharpShort2.distance();
  int distance3 = sharpShort3.distance();
  int difference = distance1-distance3;
  int lastDifference = difference;
  int i=0;
  int originalSpeed = fullSpeed;

  sample1.clear();
  sample2.clear();
  sample3.clear();
  for(i=0; i<samplingSize; i++){
    distance1 = sharpShort1.distance();
    distance2 = sharpShort2.distance();
    distance3 = sharpShort3.distance();
    sample1.add(distance1);
    sample2.add(distance2);
    sample3.add(distance3);
  }
  distance1 = sample1.getMedian();
  distance2 = sample2.getMedian();
  distance3 = sample3.getMedian();
  difference = distance1-distance3;
    
  while (lastDifference != 0 || sample2.getMedian() != 8){
    sample1.clear();
    sample2.clear();
    sample3.clear();
    for(i=0; i<samplingSize; i++){
      distance1 = sharpShort1.distance();
      distance2 = sharpShort2.distance();
      distance3 = sharpShort3.distance();
      sample1.add(distance1);
      sample2.add(distance2);
      sample3.add(distance3);
    }
    distance1 = sample1.getMedian();
    distance2 = sample2.getMedian();
    distance3 = sample3.getMedian();
    lastDifference = difference;
    difference = distance1-distance3;

    if(!left){
      if(distance2 > 8)
        md.setM1Speed(100);
      else if(distance2 < 8)
        md.setM1Speed(-100);
      if(distance1 > 8)
        md.setM1Speed(100);
      else if(distance2 < 8)
        md.setM1Speed(-100);
    }else if(!right){
      if(distance2 > 8)
        md.setM1Speed(100);
      else if(distance2 < 8)
        md.setM1Speed(-100);
      if(distance3 > 8)
        md.setM1Speed(100);
      else if(distance2 < 8)
        md.setM1Speed(-100);
    }else if(!middle){
      if(distance1 > 8)
        md.setM1Speed(100);
      else if(distance2 < 8)
        md.setM1Speed(-100);
      if(distance3 > 8)
        md.setM1Speed(100);
      else if(distance2 < 8)
        md.setM1Speed(-100);
    }
    delay(75);
    md.setBrakes(400, 400);   
  }
  delay(movementDelay);
  fullSpeed = 100;
  moveForward(0.18, true);
  fullSpeed = originalSpeed;
  md.setBrakes(400, 400);
  delay(movementDelay);
}

//void PWM_Mode_Setup(){ 
//  pinMode(7,OUTPUT);                     // A low pull on pin COMP/TRIG
//  digitalWrite(7,HIGH);                  // Set to HIGH
//  pinMode(6, INPUT);                      // Sending Enable PWM mode command  
//}

//int PWM_Mode(){                              // a low pull on pin COMP/TRIG  triggering a sensor reading
//    digitalWrite(7, LOW);
//    digitalWrite(7, HIGH);               // reading Pin PWM will output pulses
//     
//    unsigned long DistanceMeasured=pulseIn(6,LOW);
//
//    return (int)((DistanceMeasured>=10200) ? 0 : DistanceMeasured/50);
//}




